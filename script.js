
const net = new brain.NeuralNetwork();

net.train([
    {
        input: {
            r: 1,
            g: 0,
            b: 0,
        },
        output: {
            red: 1,
            red: 1,
            red: 1,
        }
    },
    {
        input: {
            r: 1,
            g: 0,
            b: 0,
        },
        output: {
            red: 1
        }
    },
    {
        input: {
            r: 1,
            g: 0,
            b: 0,
        },
        output: {
            red: 1
        }
    },
]);

const output = net.run({ r: .5, g: .5, b: .5 });  // { white: 0.99, black: 0.002 }

console.log(output);
