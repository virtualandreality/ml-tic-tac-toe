(function($){

    /*
    TIC TAC TOE
       Players: 0, 1
       Squares 1-9
       Square Status: -1 = blank, 0 = circles, 1 = crosses
   */

    $(document).ready(function(){
        let MyGame = TTT.InitGame();
    });

    let TTT = {

        // Elements
        $ttt: $('.tic-tac-toe'),
        $board: $('.tic-tac-toe .game .board'),
        $squares: $('.tic-tac-toe .game .board .square'),
        $scoreboard: $('.tic-tac-toe .game .score-board'),

        // Variables
        Combos: [
            [0,1,2],
            [0,4,8],
            [0,3,6],
            [1,4,7],
            [2,5,8],
            [2,4,6],
            [3,4,5],
            [6,7,8],
        ],
        PlayerStart: null,
        PlayerTurn: null,
        MoveCount: 0,

        CurrentSquare: null,
        Winner: null,

        // Record
        Moves: [],
        Rounds: [],

        // Tools
        CleanBoard: function(){
            this.$squares.removeClass('selected').attr('data-status','');
        },
        RandomPlayer: function(){
            let min = 0;
            let max = 1;
            return Math.floor(Math.random()*(max-min+1)+min);
        },

        // Events
        RegisterSquareClickEvents: function(){
            this.$squares.off().not('.selected').on('click',function(){
                TTT.SquareClicked($(this));
            });
        },

        // Game
        InitGame: function(){

            this.PlayerStart = this.RandomPlayer();
            this.PlayerTurn = this.PlayerStart;

            this.Scores = {
                rounds: 0,
                0: 0,
                1: 0,
            };

            $('.new-game').off().on('click',function(){
                TTT.CleanBoard();
                TTT.StartGame();
            });
        },
        StartGame: function(){

            $('.round-result .result').empty();

            this.CurrentSquare = null;
            this.Winner = null;
            this.MoveCount = 0;
            this.Moves = [];
            this.RegisterSquareClickEvents();
        },
        SquareClicked: function($this){

            // update variables
            this.MoveCount++;
            this.CurrentSquare = parseInt($this.attr('data-id'));

            // update html
            $this.attr('data-status',TTT.PlayerTurn).addClass('selected');

            // check game status
            let result;
            if(result = TTT.CheckGameFinished())
                TTT.GameFinish(result);
            else
                TTT.RegisterSquareClickEvents();

            this.EndPlayerTurn();
        },
        EndPlayerTurn:function(){
            this.RecordMove();
            TTT.PlayerTurn = !TTT.PlayerTurn ? 1 : 0;
        },
        CheckGameFinished: function(){

            let result = false;

            // Cycle players
            [0,1].forEach(function(player){

                // Group players squares
                let squares = [];
                TTT.$board.find('.square[data-status="'+player+'"]').each(function(){
                    squares.push(parseInt($(this).attr('data-id')))
                });

                // Player made line? -> Return Winner
                TTT.Combos.forEach(function(Combo){
                    let matches = [];
                    Combo.forEach(function(num){
                        if(squares.includes(num)){
                            matches.push(num)
                        }
                    });
                    if(matches.length >= 3){
                        result = [player,matches];
                    }
                });

                // All blocks used?
                if(!result && TTT.$board.find('.square.selected').length >= 9){
                    result = [-1];
                }
            });
            return result;
        },
        GameFinish: function(result){

            this.$squares.off();
            this.Scores.rounds += 1;
            $result = $('.round-result .result');

            let score;
            if(result[0] < 0){// null
                $result.text('No Winner!')
            }
            else if(!result[0]){// p0
                score = parseInt(this.Scores['0']);
                this.Scores['0'] = score+1;
                $result.text('P0 Wins!')
            }
            else if(result[1]){// p1
                score = parseInt(this.Scores['1']);
                this.Scores['1'] = score+1;
                $result.text('P1 Wins!')
            }
            this.Winner = result[0];

            this.RecordRound();
            this.ShowScores();
        },
        ShowScores: function(){

            Object.keys(TTT.Scores).forEach(function(key,ind){
                $('input[data-selector="'+key+'"]').val(TTT.Scores[key]);
            });
        },

        // ML
        RecordMove: function(){

            this.Moves.push({
                playerTurn: this.PlayerTurn,
                Square: this.CurrentSquare,
            });
        },
        RecordRound: function(){

            this.Rounds.push({
                Moves: this.Moves,
                Winner: this.Winner,
            });

            console.log(this.Rounds);
        },

    };

})(jQuery);