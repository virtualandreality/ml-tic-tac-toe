<!DOCTYPE html>
<html>
<head>
	<title>Brain</title>
    <link rel="stylesheet" type="text/css" href="tic-tac-toe.css">
</head>
<body>

	<div class="tic-tac-toe">

        <div class="header">
            <h1>TIC TAC TOE</h1>
            <p>
                Simple game of Tic-Tac-Toe (for Machine Learning experimentation purposes)<br>
                Click 'New Game' to get started.<br>
                Take turns to select boxes, 3 in a row will determine the winner.
            </p>
        </div>


		<div class="controls">
			<button class="new-game">New Game</button>
		</div>


		<div class="game">

            <div class="round-result">
                <label>Round Result:</label>
                <p class="result"></p>
            </div>

            <div class="score-board">
                <h4>Scores</h4>
                P0:<input type="number" data-selector="0" value="0">
                P1:<input type="number" data-selector="1" value="0">
                <h4>Rounds</h4>
                <input type="number" data-selector="rounds" value="0">
            </div>

			<div class="board" style="max-width: 320px;">
				<?php
				$i = 0;
				while($i < 9):?>
					<div class="square" data-id="<?= $i;?>"></div>
					<?php
					$i++;
				endwhile?>
			</div>

        </div>

	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="tic-tac-toe.js"></script>
	<script type="text/javascript" src="brain.js"></script>
</body>
</html>